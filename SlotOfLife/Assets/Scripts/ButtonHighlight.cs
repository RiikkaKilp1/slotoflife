﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class ButtonHighlight : MonoBehaviour
{
    public Text theText;
    public Color Highlight;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("here");
        theText.color = Highlight; //Or however you do your color
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        theText.color = Color.green; //Or however you do your color
    }
}
