﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public enum WinState {
    DeathDeathFULL,
    DeathDeathHALF,
    DeathFULL,
    DeathHALF,
    CoinFULL,
    CoinHALF,
    LifeFULL,
    LifeHALF,
    LifeLifeFULL,
    LifeLifeHALF,
    NoWin }

public class SlotMachine : MonoBehaviour
{
    public Text soulDisplay, resultText;

    public GameObject MainGame, GameOver, Prizes, ToggleElements;

    public Text endStatus;

    public float startSouls,
                 winSouls,
                 loseSouls,
                 currentSouls;

    public WinState win { get; private set; }

    public GameObject nextButton, nextEndButton, winButton;
    public List<GameObject> rollStorage = new List<GameObject>();

    public string[] fillerLines, startLines, endLines, winLines;

    public GameObject machine;

    public GameObject[] slotRolls;
    public Transform[] slotPlacement;

    private Animator ac;
    private bool firstTime, gameLost, gameWon, gameOn;

    //The slotObject to be presented on the column
    int r;

    int diaNum;
    public Button playButton;
    bool gameOver = false;

    bool prizesOn, gameScreenOn;

    // Start is called before the first frame update
    void Start()
    {

        winButton.SetActive(false);
        playButton.enabled = false;
        nextEndButton.SetActive(false);

        nextButton.SetActive(true);

        gameOn = true;
        ac = machine.GetComponent<Animator>();
        firstTime = true;
        currentSouls = startSouls;
        soulDisplay.text = "x " + currentSouls;


        StartDialogue();        
    }

    private void Update()
    {

        // If game lost
        if (!gameOver)
        {
            if (currentSouls <= loseSouls || gameLost)
            {
                Debug.Log("game lost");
                EndtDialogue();
                nextEndButton.SetActive(true);
                Button button = machine.GetComponentInChildren<Button>();
                button.enabled = false;
                gameLost = true;
                gameOver = true;
            }

            if (currentSouls >= winSouls || gameWon)
            {
                Debug.Log("game won");
                WinDialogue();
                winButton.SetActive(true);
                Button button = machine.GetComponentInChildren<Button>();
                button.enabled = false;
                gameWon = true;
                gameOver = true;
            }
        }

    }

    public void LeverPulled()
    {
        // Random filler line for the pull
        int rFiller = Random.Range(0, fillerLines.Length);
        string thisFiller = fillerLines[rFiller];
        resultText.text = thisFiller;

        currentSouls = currentSouls - 1;

        //Empty the previous result
        if(rollStorage.Count > 0 && !firstTime)
        {
            for(int i = 0; i < rollStorage.Count; i++)
            {
                
                Animator tempAC = rollStorage[i].GetComponent<Animator>();
                tempAC.Play("A_Out_01");
                Destroy(rollStorage[i], 1);
                
            }

            rollStorage.Clear();
        }

        firstTime = false;

        StartCoroutine(Delay(0.9f));
        
    }

    IEnumerator Delay(float delTime)
    {
        yield return new WaitForSeconds(delTime);

        //Get a new result;
        for (int i = 0; i < slotPlacement.Length; i++)
        {
            r = Random.Range(0, 4);
            GameObject tempRoll = Instantiate(slotRolls[r], slotPlacement[i]);
            tempRoll.transform.SetSiblingIndex(0);
            Animator tempAc = tempRoll.GetComponent<Animator>();
            tempAc.Play("A_In_01");
            rollStorage.Add(tempRoll);
        }

        CheckResult();
    }

    public void CheckResult()
    {
        //Check if DeathDeath results
        if (rollStorage[0].tag == "DeathDeath" &&
            rollStorage[1].tag == "DeathDeath" &&
            rollStorage[2].tag == "DeathDeath")
        {
            win = WinState.DeathDeathFULL;
            gameLost = true;
            Debug.Log("DeathDeath FULL");
        }

        else if (rollStorage[0].tag == "DeathDeath" &&
                 rollStorage[1].tag == "DeathDeath" ||

                 rollStorage[1].tag == "DeathDeath" &&
                 rollStorage[2].tag == "DeathDeath" ||

                 rollStorage[0].tag == "DeathDeath" &&
                 rollStorage[2].tag == "DeathDeath")
        {
            win = WinState.DeathDeathHALF;
            Debug.Log("DeathDeath HALF");
        }

        //Check if Death results
        else if (rollStorage[0].tag == "Death" &&
            rollStorage[1].tag == "Death" &&
            rollStorage[2].tag == "Death")
        {
            win = WinState.DeathFULL;
            Debug.Log("Death FULL");
        }

        else if (rollStorage[0].tag == "Death" &&
                 rollStorage[1].tag == "Death" ||

                 rollStorage[1].tag == "Death" &&
                 rollStorage[2].tag == "Death" ||

                 rollStorage[0].tag == "Death" &&
                 rollStorage[2].tag == "Death")
        {
            win = WinState.DeathHALF;
            Debug.Log("Death HALF");
        }

        //Check if Coin results
        else if (rollStorage[0].tag == "Coin" &&
            rollStorage[1].tag == "Coin" &&
            rollStorage[2].tag == "Coin")
        {
            win = WinState.CoinFULL;
            Debug.Log("Coin FULL");
        }

        else if (rollStorage[0].tag == "Coin" &&
                 rollStorage[1].tag == "Coin" ||
                                        
                 rollStorage[1].tag == "Coin" &&
                 rollStorage[2].tag == "Coin" ||
                                        
                 rollStorage[0].tag == "Coin" &&
                 rollStorage[2].tag == "Coin")
        {
            win = WinState.CoinHALF;
            Debug.Log("COIN HALF");
        }

        //Check if Life results
        else if (rollStorage[0].tag == "Life" &&
            rollStorage[1].tag == "Life" &&
            rollStorage[2].tag == "Life")
        {
            win = WinState.LifeFULL;
            Debug.Log("Life FULL");
        }

        else if (rollStorage[0].tag == "Life" &&
                 rollStorage[1].tag == "Life" ||
                                        
                 rollStorage[1].tag == "Life" &&
                 rollStorage[2].tag == "Life" ||
                                        
                 rollStorage[0].tag == "Life" &&
                 rollStorage[2].tag == "Life")
        {
            win = WinState.LifeHALF;
            Debug.Log("Life HALF");
        }

        //Check if LifeLife results
        else if (rollStorage[0].tag == "LifeLife" &&
            rollStorage[1].tag == "LifeLife" &&
            rollStorage[2].tag == "LifeLife")
        {
            win = WinState.LifeLifeFULL;
            Debug.Log("LifeLife FULL");
        }

        else if (rollStorage[0].tag == "LifeLife" &&
                 rollStorage[1].tag == "LifeLife" ||
                                        
                 rollStorage[1].tag == "LifeLife" &&
                 rollStorage[2].tag == "LifeLife" ||
                                        
                 rollStorage[0].tag == "LifeLife" &&
                 rollStorage[2].tag == "LifeLife")
        {
            win = WinState.LifeLifeHALF;
            gameWon = true;
            Debug.Log("LifeLife HALF");
        }

        else
        {
            win = WinState.NoWin;
        }

        StartCoroutine(ResultDelay(1f));
    }

    IEnumerator ResultDelay(float resultTime)
    {
        yield return new WaitForSeconds(resultTime);

        float tempSouls;
        float soulDiff;

        switch (win)
        {
            case WinState.DeathDeathFULL:
                resultText.text = "You lost!";
                break;

            case WinState.DeathDeathHALF:
                tempSouls = currentSouls;
                currentSouls = currentSouls - (currentSouls * 0.75f);
                soulDiff = tempSouls - currentSouls;
                soulDiff = Mathf.Ceil(soulDiff);
                resultText.text = "Oof, two double crosses! That's, like, really bad. You lost " + soulDiff + " souls.";
                break;

            case WinState.DeathFULL:
                tempSouls = currentSouls;
                currentSouls = currentSouls - (currentSouls * 0.5f);
                soulDiff = tempSouls - currentSouls;
                soulDiff = Mathf.Ceil(soulDiff);
                resultText.text = "That stings. Three crosses, so you lost " + soulDiff + " souls. What a shame";
                break;

            case WinState.DeathHALF:
                tempSouls = currentSouls;
                currentSouls = currentSouls - (currentSouls * 0.25f);
                soulDiff = tempSouls - currentSouls;
                soulDiff = Mathf.Ceil(soulDiff);
                resultText.text = "You can't win without sacrificing something, right? Losing only " + soulDiff + " of the souls is not that bad. Oh, but you only have one of your own. Oops.";
                break;

            case WinState.CoinFULL:
                resultText.text = "I didn't come up with anything, I thought the coins looked cool. Sorry ;3";
                break;

            case WinState.CoinHALF:
                resultText.text = "Just one more for something epic! So close !!";
                break;

            case WinState.LifeHALF:
                tempSouls = currentSouls;
                currentSouls = currentSouls + (currentSouls * 0.25f);
                soulDiff = currentSouls -tempSouls;
                soulDiff = Mathf.Ceil(soulDiff);
                resultText.text = "Every bit counts! All those souls you could have won and you only got " + soulDiff + ".";
                break;

            case WinState.LifeFULL:
                tempSouls = currentSouls;
                currentSouls = currentSouls + (currentSouls * 0.5f);
                soulDiff = currentSouls - tempSouls;
                soulDiff = Mathf.Ceil(soulDiff);
                resultText.text = "Three hearts! Almost as many as I eat for breakfast. That means you get " + soulDiff + " souls. Not bad.";
                break;

            case WinState.LifeLifeHALF:
                tempSouls = currentSouls;
                currentSouls = currentSouls + (currentSouls * 0.75f);
                soulDiff = currentSouls -tempSouls;
                soulDiff = Mathf.Ceil(soulDiff);
                resultText.text = "Two double hearts! It's such a shame you humans only have one. But with these doubles, you got " + soulDiff + " souls.";
                break;

            case WinState.LifeLifeFULL:
                resultText.text = "Instant win! You got really lucky, like, too lucky... I think this might be rigged...";
                break;

            case WinState.NoWin:
                resultText.text = "No win! Also, you lost nothing, so I guess this counts as a win? For you, at least.";
                break;

        }

        currentSouls = Mathf.Ceil(currentSouls);
        soulDisplay.text = "x " + currentSouls;
    }

    public void LeverAnimation()
    {
        ac.Play("A_HandPull_01");
    }

    public void StartDialogue()
    {
        resultText.text = startLines[diaNum];
        diaNum++;

        if (diaNum >= startLines.Length)
        {
            Debug.Log("Got here");
            nextButton.SetActive(false);
            playButton.enabled = true;
            diaNum = 0;
        }

    }

    public void EndtDialogue()
    {
        resultText.text = endLines[diaNum];
        diaNum++;

        if (diaNum >= endLines.Length)
        {
            nextButton.SetActive(false);
            playButton.enabled = true;
            MainGame.SetActive(false);
            GameOver.SetActive(true);

            endStatus.text = "You lost. Again";
        }

    }

    public void WinDialogue()
    {
        resultText.text = winLines[diaNum];
        diaNum++;

        if (diaNum >= winLines.Length)
        {
            winButton.SetActive(false);
            playButton.enabled = true;
            MainGame.SetActive(false);
            GameOver.SetActive(true);

            endStatus.text = "You beat the devil!";
        }

    }

    public void ManualToggle()
    {
        if (ToggleElements.activeSelf)
        {
            ToggleElements.SetActive(false);
            Prizes.SetActive(true);
        }

        else if (Prizes.activeSelf)
        {
            ToggleElements.SetActive(true);
            Prizes.SetActive(false);
        }
    }



}
